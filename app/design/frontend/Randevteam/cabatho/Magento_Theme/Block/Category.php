<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magento\Theme\Magento_Theme\Block\Html;

/**
 * Html page header block
 *
 * @api
 * @since 100.0.2
 */
class Category extends Magento\Framework\View\Element\Template
{
    /**
     * Current template name
     *
     * @var string
     */
    protected $_template = 'html/category.phtml';

    /**
     * Retrieve welcome text
     *
     * @return string
     */

    public function getWelcome()
    {
        echo "yhy";
    }
}
