<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magento\Theme\Magento_Theme\Block\Html;

/**
 * Html page header block
 *
 * @api
 * @since 100.0.2
 */
class Category extends \Magento\Framework\View\Element\Template
{
    /**
     * Current template name
     *
     * @var string
     */
    protected $_template = 'html/category.phtml';

    /**
     * Retrieve welcome text
     *
     * @return string
     */
    public function __construct(
        \Magento\Catalog\Helper\Category $category,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository
    ) {
        $this->category = $category;
        $this->categoryRepository = $categoryRepository;
    }
    public function getWelcome()
    {
        $categories = $this->category->getStoreCategories();
        foreach($categories as $category) {
            echo $category->getName().'<br/>';
            $categoryObj = $this->categoryRepository->get($category->getId());
            $subcategories = $categoryObj->getChildrenCategories();
            foreach($subcategories as $subcategorie) {
                echo '    --> '.$subcategorie->getName().'<br/>';
            }
        }
    }
}
